; ## Global constants ##
STR_MAX_SZ	EQU 21		;Max string buffer size
TAB			EQU 09h		;Tab
NL			EQU 0Dh,0Ah	;New line

; ## External prototypes ##
ExitProcess PROTO
ReadInt64 PROTO
WriteInt64 PROTO
WriteHex64 PROTO
ReadString PROTO
WriteString PROTO
Crlf PROTO

.data
	rowSz qword 40
	arr qword	00, 01, 02, 03, 04,
				05, 06, 07, 08, 09,
				10, 11, 12, 13, 14

	msg01 byte "(0,1): ",0
	msg06 byte "(1,2): ",0
	msg12 byte "(2,3): ",0

.code
Main PROC
;(0,1)	
	mov rdx, offset msg01
	call WriteString
	call Crlf

	mov rdx, offset arr			;array base pointer
	mov rsi, 1					;row
	mov rdi, 3					;col

	imul rsi, rsi, 40		;include previous rows
	mov rbx, rsi				;set row

	imul rdi, rdi, type rdx		;establish column
	add rbx, rdi				;add the columns

	mov rax, [rdx + rbx]

;	imul rax, [rdx + (type rdx)*rdi],5

	call WriteInt64

	
	call Crlf
	call ExitProcess
Main ENDP

PUBLIC Main
END
